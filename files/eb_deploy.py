#!/usr/bin/python

from __future__ import print_function
import os
import sys
import boto3
from botocore.exceptions import ClientError
import argparse


def deploy_new_version(application_name, application_environment, artifact):
    try:
        client = boto3.client('elasticbeanstalk')
    except ClientError as err:
        print("Failed to create boto3 client.\n" + str(err))
        return False

    try:
        response = client.update_environment(
            ApplicationName=application_name,
            EnvironmentName=application_environment,
            VersionLabel=os.path.basename(artifact).split("-", 1)[1].split(".zip")[0],
        )
    except ClientError as err:
        print("Failed to update environment.\n" + str(err))
        return False

    print(response)
    return True

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("artifact")
    parser.add_argument("application_name")
    parser.add_argument("application_environment")
    args = parser.parse_args()

    if not deploy_new_version(args.application_name, args.application_environment, args.artifact):
        sys.exit(1)

if __name__ == "__main__":
    main()
