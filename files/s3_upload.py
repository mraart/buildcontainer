#!/usr/bin/python

from __future__ import print_function
import os
import sys
import boto3
from botocore.exceptions import ClientError
import argparse


def upload_to_s3(application_name, artifact, bucket):
    key = os.path.basename(artifact)
    try:
        client = boto3.client('s3')
    except ClientError as err:
        print("Failed to create boto3 client.\n" + str(err))
        return False

    try:
        response = client.put_object(
            Body = open(artifact, 'rb'),
            Bucket = bucket,
            Key = application_name + "/" + key
        )
    except ClientError as err:
        print("Failed to upload artifact to S3.\n" + str(err))
        return False
    except IOError as err:
        print("Failed to access " + artifact + " in this directory.\n" + str(err))
        return False
    try:
        if response['ResponseMetadata']['HTTPStatusCode'] is 200:
            return True
        else:
            print(response)
            return False
    except (KeyError, TypeError) as err:
        print(str(err))
        return False

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("artifact")
    parser.add_argument("application_name")
    parser.add_argument("bucket")
    args = parser.parse_args()


    if not upload_to_s3(args.application_name, args.artifact, args.bucket):
        sys.exit(1)

if __name__ == "__main__":
    main()
