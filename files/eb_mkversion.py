#!/usr/bin/python

from __future__ import print_function
import os
import sys
from time import strftime, sleep
import boto3
from botocore.exceptions import ClientError
import argparse


def create_new_version(application_name, artifact, bucket):
    try:
        client = boto3.client('elasticbeanstalk')
    except ClientError as err:
        print("Failed to create boto3 client.\n" + str(err))
        return False

    try:
        response = client.create_application_version(
            ApplicationName=application_name,
            VersionLabel=os.path.basename(artifact).split("-", 1)[1].split(".zip")[0],
            Description='New build from Bitbucket',
            SourceBundle={
                'S3Bucket': bucket,
                'S3Key': application_name + "/" +os.path.basename(artifact)
            },
            Process=True
        )
    except ClientError as err:
        print("Failed to create application version.\n" + str(err))
        return False

    try:
        if response['ResponseMetadata']['HTTPStatusCode'] is 200:
            return True
        else:
            print(response)
            return False
    except (KeyError, TypeError) as err:
        print(str(err))
        return False

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("artifact")
    parser.add_argument("application_name")
    parser.add_argument("bucket")
    args = parser.parse_args()


    if not create_new_version(args.application_name, args.artifact, args.bucket):
        sys.exit(1)
    # Wait for the new version to be consistent before deploying
    sleep(5)


if __name__ == "__main__":
    main()
